package com.techdevbrazil.exemplouuid;

import com.techdevbrazil.exemplouuid.entity.Person;
import com.techdevbrazil.exemplouuid.service.PersonService;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;


@SpringBootApplication
public class ExemploUuidApplication {

	private static final Logger Log = LoggerFactory.getLogger(ExemploUuidApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ExemploUuidApplication.class, args);
	}


	@Bean
	public CommandLineRunner demo(PersonService personService) {
		return (arg) -> {


			Log.info("Running save method");
			personService.save(new Person("Joseph", 30));
			personService.save(new Person("Mary", 23));


			Log.info("Running findAll method");
			List<Person> list = personService.finAll();
			Log.info("List" + list);

			/*Log.info("Running deleting method");
			personService.deletAll(list);*/


		};
	}
}
