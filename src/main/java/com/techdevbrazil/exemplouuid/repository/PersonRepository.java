package com.techdevbrazil.exemplouuid.repository;

import com.techdevbrazil.exemplouuid.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Long> {
}
