package com.techdevbrazil.exemplouuid.service;

import com.techdevbrazil.exemplouuid.entity.Person;
import com.techdevbrazil.exemplouuid.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public List<Person> finAll(){
        return personRepository.findAll();
    }

    public Person save(Person person){
        return personRepository.save(person);
    }

    public void deletAll(List<Person> list){
        for (Person person : list){
            personRepository.delete(person);
        }
    }

}
